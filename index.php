<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Descuento</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="icons/css/all.min.css">
</head>

<body>
    <header>
        <div>
            <img src="images/logo1.png" alt="Logo de supertiernos">
        </div>
        <div>
            <h1>Supertiernos RG</h1>
        </div>
    </header>

    <div id="banner-landing">
        <!-- <img src="images/Banner222.jpg" alt="Banner de promocion de temporada"> -->
        <div>El Detalle Ideal</div>
        <div>Para tu persona Favorita</div>
        <br>
        <div>Reclama tu Descuento</div>
        <div><button id="reclamar">20 %</button> </div>
    </div>


    <!-- contador de tiempo -->
    <div class="temporizador">
		<div class="titulo">
			<div> <h1>Rapido!!</h1></div>
        	<div><h2>Esta oferta Termina en:</h2></div>
		</div>

        <div class="tiempo" >
			<div class="bloque">
				<div class="dias" id="dias">0</div>
				<p>DIAS</p>
			</div>
			<div class="bloque">
				<div class="horas" id="horas">--</div>
				<p>HORAS</p>
			</div>
			<div class="bloque">
				<div class="minutos" id="minutos">--</div>
				<p>MINUTOS</p>
			</div>
			<div class="bloque">
				<div class="segundos" id="segundos">--</div>
				<p>SEGUNDOS</p>
			</div>
		</div>
    </div>
    <script src="temporizador.js"></script>
    

    <!-- Cuerpo-->
    <div id="contenedor-titulo"> <!-- Contenedor del titulo -->
        <h1>Nuestros Productos </h1>
    </div>
    <?php
        $productos=["Diseños Propios", "Peluches", "Cojines", "Mugs", "Anchetas"];
    ?> 

    <div class="contenedor-tarjetas">  <!-- Contenedor de tarjetas -->
        <div class="tarjetas"> <!-- tarjeta 1 -->
            <img src="images/jirafa.jpg" alt="Imagen de Peluche">
            <h3> 
                <?php
                    echo $productos[0];
                ?>
            </h3>
            <button class="btn-tarjeta">Ver Mas</button>
        </div>
        <div class="tarjetas">  <!-- tarjeta 2 -->
            <img src="images/peluches.jpg" alt="Imagen de Peluche">
            <h3>
                <?php
                    echo $productos[1];
                ?>
            </h3>
            <button class="btn-tarjeta">Ver Mas</button>
        </div>
        <div class="tarjetas">  <!-- tarjeta 3 -->
            <img src="images/cojines.jpg" alt="Imagen de Peluche">
            <h3>
                <?php
                    echo $productos[2];
                ?>
            </h3>
            <button class="btn-tarjeta">Ver Mas</button>
        </div>
        <div class="tarjetas">  <!-- tarjeta 4 -->
            <img src="images/mugs.jpg" alt="Imagen de Peluche">
            <h3>
                <?php
                    echo $productos[3];
                ?>
            </h3>
            <button class="btn-tarjeta">Ver Mas</button>
        </div>

       

        <div class="tarjetas" >  <!-- tarjeta formulario -->
            <h3>Quiero Saber Mas:</h3>
            <div id="contenedor-formulario">
            
                <form class="formulario" action="proceso.php" method="POST ">
                    <label for="Nombre">Nombre</label>
                    <input type="text" id="" name="nombre" id="nombre" placeholder="Nombre">

                    <label for="correo">Correo</label>
                    <input type="email" name="email" id="email" placeholder="Correo">

                    <label for="Asunto">Asunto</label>
                    <textarea name="asunto" id="" cols="25" rows="5" placeholder="Asunto"></textarea>
                    <br>
                    <button class="btn-tarjeta" type="submit">Enviar</button>
                </form>
            </div>            
        </div>        
       
    </div>

    <footer class="footer">
        <h2>Nuestras Redes Sociales</h2>
        <div class="footer__links">
            <a href="https://es-la.facebook.com/" class="footer__links" target="_blank"><i class="fab fa-facebook-f"></i></a>
            <a href="https://twitter.com/" class="footer__links" target="_blank"><i class="fab fa-twitter"></i></a>
            <a href="https://api.whatsapp.com/send?phone=3204546414" class="footer__links" target="_blank"><i class="fab fa-whatsapp"></i></a>
       </div>
    </footer>

</body>

</html>