class Usuario {
    constructor(id, nombre, clave, token) {
        this.id = id;
        this.nombre = nombre;
        this.clave = clave;
        this.token = token;
    }
    getId() {
        return this.id;
    }
    setId(id_nuevo) {
        this.id = id_nuevo;
    }

    getNombre() {
        return this.nombre;
    }
    setNombre(nombre_nuevo) {
        this.nombre = nombre_nuevo;
    }

    getClave() {
        return this.clave;
    }
    setClave(clave_nuevo) {
        this.clave = clave_nuevo;
    }

    getToken() {
        return this.token;
    }
    setToken(token_nuevo) {
        this.token = token_nuevo;
    }
}

class Cliente {
    constructor(id, nombre, cedula, email) {
        this.id = id;
        this.nombre = nombre;
        this.cedula = cedula;
        this.email = email;
    }
    getId() {
        return this.id;
    }
    setId(id_nuevo) {
        this.id = id_nuevo;
    }

    getNombre() {
        return this.nombre;
    }
    setNombre(nombre_nuevo) {
        this.nombre = nombre_nuevo;
    }

    getCedula() {
        return this.cedula;
    }
    setCedula(cedula_nuevo) {
        this.cedula = cedula_nuevo;
    }
    getEmail() {
        return this.email;
    }
    setEmail(email_nuevo) {
        this.email = email_nuevo;
    }
}

class Proveedor {
    constructor(id, nombre, empresa, direccion) {
        this.id = id;
        this.nombre = nombre;
        this.empresa = empresa;
        this.direccion = direccion
    }

    getId() {
        return this.id;
    }
    setId(id_nuevo) {
        this.id = id_nuevo;
    }

    getNombre() {
        return this.nombre;
    }
    setNombre(nombre_nuevo) {
        this.nombre = nombre_nuevo;
    }

    getEmpresa() {
        return this.empresa;
    }
    setEmpresa(empresa_nuevo) {
        this.empresa = empresa_nuevo;
    }

    getDireccion() {
        return this.Direccion;
    }
    setEmpresa(direccion_nuevo) {
        this.direccion = direccion_nuevo;
    }
}

/* Terminar */
// Clase
// constructor
// getters and setters
// methods

class Producto {
    constructor(id, nombre, marca, categoria, stock) {
        this.id = id;
        this.nombre = nombre;
        this.marca = marca;
        this.categoria = categoria;
        this.stock = stock;
    }

    getId() {
        return this.id;
    }
    setId(id_nuevo) {
        this.id = id_nuevo;
    }

    getNombre() {
        return this.nombre;
    }
    setNombre(nombre_nuevo) {
        this.nombre = nombre_nuevo;
    }

    getMarca() {
        return this.marca;
    }
    setMarca(marca_nuevo) {
        this.marca = marca_nuevo;
    }

    getCategoria() {
        return this.categoria;
    }
    setCategoria(categoria_nuevo) {
        this.categoria = categoria_nuevo;
    }
    getStock() {
        return this.stock;
    }
    setStock(stock_nuevo) {
        this.stock = stock_nuevo;
    }
}

class Factura {
    constructor(id, fecha, id_Vendedor, id_Cliente, id_Producto, cantidad, impuesto) {
        this.id = id;
        this.fecha = fecha;
        this.id_Vendedor = id_Vendedor;
        this.id_Cliente = id_Cliente;
        this.iid_Producto = id_Producto;
        this.cantidad = cantidad;
        this.impuesto = impuesto;
    }

    getId() {
        return this.id;
    }
    setId(id_nuevo) {
        this.id = id_nuevo;
    }

    getFecha() {
        return this.fecha;
    }
    setFecha(fecha_nuevo) {
        this.fecha = fecha_nuevo;
    }

    getId_Vendedor() {
        return this.id_Vendedor;
    }
    setId_Vendedor(id_Vendedor_nuevo) {
        this.id_Vendedor = id_Vendedor_nuevo;
    }

    getId_Cliente() {
        return this.id_Cliente;
    }
    setId_Cliente(id_Cliente_nuevo) {
        this.id_Cliente = id_Cliente_nuevo;
    }

    getId_Producto() {
        return this.id_producto;
    }
    setId_Producto(id_producto_nuevo) {
        this.id_producto = id_producto_nuevo;
    }

    getCantidad() {
        return this.cantidad;
    }
    setCantidad(cantidad_nuevo) {
        this.cantidad = cantidad_nuevo;
    }

    getImpuesto() {
        return this.impuesto;
    }
    setImpuesto(impuesto_nuevo) {
        this.impuesto = impuesto_nuevo;
    }
}
class empleado {
    constructor(id, nombre, clave, rol) {
        this.nombre = nombre;
        this.clave = clave;
        this.rol = rol;
        this.id = id;
    }
    getId() {
        return this.id;
    }
    setId(id_nuevo) {
        this.id = id_nuevo;
    }

    getNombre() {
        return this.nombre;
    }
    setNombre(nombre_nuevo) {
        this.nombre = nombre_nuevo;
    }

    getClave() {
        return this.clave;
    }
    setClave(clave_nuevo) {
        this.clave = clave_nuevo;
    }

    getRol() {
        return this.rol;
    }
    setRol(rol_nuevo) {
        this.rol = rol_nuevo;
    }
}